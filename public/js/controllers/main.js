'use strict';

angular.module('todoListApp')
.controller('mainCtrl', function($scope, $log, $interval, dataService) {

/*
	// Example usage of Angular's use of native JS functionality
	$scope.seconds = 0;
	$scope.counter = function() {
		$scope.seconds++;
		$log.log($scope.seconds + ' seconds have passed');
	}
	$interval($scope.counter, 1000, 10);
*/
	// read
	$scope.todos = [];
	dataService.getTodos(function(response) {
		$scope.todos = response.data.todos;
	});

	// add new unsaved todo to the list
	$scope.addTodo = function() {
		var todo = {name: "A new todo item"}
		$scope.todos.unshift(todo); // add to front of the array
	};

	// save todo
	$scope.saveTodos = function(todo) {
		var filteredTodos = $scope.todos.filter(function(todo) {
			if(todo.edited) {
				return todo;
			}
		});
		dataService.saveTodos(filteredTodos).finally($scope.resetTodosState());
	};

	// remove edited flag from all todos
	$scope.resetTodosState = function() {
		$scope.todos.forEach(function(todo) {
			todo.edited = false;
		});
	}

	// delete and remove todo from list
	$scope.deleteTodo = function(todo, $index) {
		dataService.deleteTodo(todo);
		$scope.todos.splice($index, 1);
	};

});