'use strict';

angular.module('todoListApp')
.service('dataService', function($http, $q, $log) {

	// read
	this.getTodos = function(callback) {
		$http.get('/api/todos').then(callback);
	}

	// create or update
	this.saveTodos = function(todos) {
		var queue = [];
		todos.forEach(function(todo) {
			var request;
			if(!todo._id) {
				// save new todo
				request = $http.post('/api/todos', todo);
			} else {
				// update exiting todo
				request = $http.put('/api/todos/' + todo._id, todo).then(function(result) {
					todo = result.data.todo;
					return todo;
				});
			}

			queue.push(request);
		});

		// use queue service to run all of our requests in parallel
		// returns promise
		return $q.all(queue).then(function(results) {
			$log.log(todos.length + " todos saved");
		});
	}

	// delete
	this.deleteTodo = function(todo) {
		$http.delete('/api/todo/' + todo._id).then(function(result) {
			$log.log(result.data.message);
		});
	}

});