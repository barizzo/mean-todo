'use strict';

var express = require('express');
var Todo = require('../models/todo');
//var todos = require('../../mock/todos.json');

var router = express.Router();

// Create
router.post('/todos', function(req, res) {
	var todo = req.body;
	Todo.create(todo, function(err, todo) {
		if(err) {
			// return so as to not send anymore responses
			return res.status(500).json({err: err.message});
		}
		res.send({'todo': todo, message: 'Todo Created'});
	});
});

// Read
router.get('/todos', function(req, res) {
	Todo.find({}, function(err, todos) {
		if(err) {
			return res.status(500).json({message: err.message});
		}
		res.json({todos:todos});
	});
});

// Update
router.put('/todos/:id', function(req, res) {
	var id = req.params.id;
	var todo = req.body;
	if(todo && todo._id !== id) {
		return res.status(500).json({err: "Incorrect ID"});
	}
	// use {new: true} to tell mongo to return the updated data
	Todo.findByIdAndUpdate(id, todo, {new: true}, function(err, todo) {
		if(err) {
			// return so as to not send anymore responses
			return res.status(500).json({err: err.message});
		}
		res.send({'todo': todo, message: 'Todo Updated'});
	});
});

// Delete
router.delete('/todo/:id', function(req, res) {
	var id = req.params.id;

	// also see findByIdAndRemove
	Todo.remove({_id: id}, function(err) {
		if(err) {
			return res.status(500).json({err: err.message});
		}
		res.send({message: 'Todo has been deleted'});
	});
});

module.exports = router;
