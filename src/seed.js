'use strict';

var Todo = require('./models/todo.js');
var todos = [
	"Buy milk",
	"Water the cat",
	"Feed the lawn",
	"Book the flights",
	"Finish learning the MEAN stack"
];

todos.forEach(function(todo, index) {
	Todo.find({'name': todo}, function(err, todos) {
		if(!err && !todos.length) {
			Todo.create({completed: false, name: todo});
		}
	});
});
